﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Drawing.Drawing2D;
using Word = Microsoft.Office.Interop.Word;




namespace NDSS.Forms
{
    public partial class Form3 : Form
    {
        private double[] values = { 0, 0.05, 0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4, 0.45, 0.5, 0.55, 0.6, 0.65, 0.7, 0.75, 0.8, 0.85, 0.9, 0.95, 1 };

        int count = 0;
        int transValue = 127;
        List<string>[] inputData;
        List<string> ids;
        List<string> DTW;
        List<string> TDS;
        List<string> Slope;
        List<string> ET;
        List<string> Sus;
        List<string> Existing;
        List<string> currentData;



        Point SelectedPixel = new Point(-9, -9);

        public Form3()
        {
            InitializeComponent();
            inputData = readMultiColumn(NDSS.Properties.Resources.input, 6);
            ids = idReader(@"C:\Users\Public\DSS\extracted_ids.csv");

            DTW = inputData[0];
            TDS = inputData[1];
            Slope = inputData[2];
            ET = inputData[3];
            Sus = inputData[4];
            Existing = inputData[5];

            using (var bmpTemp = new Bitmap(@"C:\Users\Public\DSS\extracted_img_Sat.png"))
            {
                double inputWidth = bmpTemp.Width;
                double inputHeight = bmpTemp.Height;

                if (inputWidth > inputHeight)
                {
                    pictureBox1.Height = (int)((inputHeight / inputWidth) * pictureBox1.Width);
                    pictureBox2.Height = pictureBox1.Height;

                    Point newLocation = new Point(pictureBox1.Location.X, pictureBox1.Location.Y + 220 - (int)(pictureBox1.Height / 2));

                    pictureBox1.Location = newLocation;
                    pictureBox2.Location = newLocation;
                }

                if (inputHeight > inputWidth)
                {
                    pictureBox1.Width = (int)((inputWidth / inputHeight) * pictureBox1.Height);
                    pictureBox2.Width = pictureBox1.Width;

                    Point newLocation = new Point(pictureBox1.Location.X + 220 - (int)(pictureBox1.Width / 2), pictureBox1.Location.Y);

                    pictureBox1.Location = newLocation;
                    pictureBox2.Location = newLocation;
                }

            }

            comboBox1.SelectedIndex = 0;
            comboBox1_SelectedIndexChanged(null, null);
            btnExport.BackColor = ColorTranslator.FromHtml(ThemeColor.ColorList[13]);
        }



        public List<string> idReader(string filePath)
        {
            if (!Directory.Exists(@"C:\Users\Public\DSS"))
            {
                MessageBox.Show("Please select area first");
                List<string> emptyString = new List<string>();
                for (int i = 0; i < 400; i++)
                {
                    emptyString.Add("43600");
                }
                return emptyString;
            }
            using (var reader = new StreamReader(filePath))
            {
                List<string> idValues = new List<string>();

                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();

                    var values = line.Split(',');
                    if (line != "")
                    {
                        idValues.Add(values[0]);
                    }

                }
                return idValues;
            }
        }

        public List<string>[] readMultiColumn(string myData, int numberOfColumns)
        {
            using (var reader = new StringReader(myData))
            {
                List<string>[] totalData = new List<string>[numberOfColumns];

                for (int i = 0; i < numberOfColumns; i++)
                {
                    totalData[i] = new List<string>();
                }

                String line;
                while ((line = reader.ReadLine()) != null)
                {
                    var values = line.Split(',');
                    if (line != "")
                    {
                        for (int i = 0; i < numberOfColumns; i++)
                        {
                            totalData[i].Add(values[i]);
                        }
                    }

                }
                return totalData;
            }
        }

        private Color getColorFromValue(double value, int trans)
        {
            if (radioButton1.Checked)
            {
                if (value > 80)
                {
                    return Color.FromArgb(trans, 0, 109, 44);
                }
                if (value > 60)
                {
                    return Color.FromArgb(trans, 44, 162, 95);
                }
                if (value > 40)
                {
                    return Color.FromArgb(trans, 102, 194, 164);
                }
                if (value > 20)
                {
                    return Color.FromArgb(trans, 178, 226, 226);
                }
                if (value >= 0)
                {
                    return Color.FromArgb(trans, 237, 248, 251);
                }
            }

            if (radioButton2.Checked)
            {
                if (value > 80)
                {
                    return Color.FromArgb(trans, 44, 123, 182);
                }
                if (value > 60)
                {
                    return Color.FromArgb(trans, 171, 217, 233);
                }
                if (value > 40)
                {
                    return Color.FromArgb(trans, 255, 255, 191);
                }
                if (value > 20)
                {
                    return Color.FromArgb(trans, 253, 174, 97);
                }
                if (value >= 0)
                {
                    return Color.FromArgb(trans, 215, 25, 28);
                }
            }

            return Color.FromArgb(trans, 255, 255, 255);
        }

        public void createImage()
        {
            int width = 10, height = 10;

            Bitmap bmp = new Bitmap(width, height);

            Random rand = new Random();

            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    int a = 255;
                    int r = rand.Next(256);
                    int g = rand.Next(256);
                    int b = rand.Next(256);

                    bmp.SetPixel(x, y, Color.FromArgb(a, 23, 179, 77));

                }
            }
            pictureBox1.Image = bmp;
        }



        private Bitmap MergeTwoImages(Image large_Image, Image small_Image)
        {
            Bitmap largeImage = (Bitmap)large_Image;
            int w1 = largeImage.Width;
            int h1 = largeImage.Height;
            Bitmap smallImage = (Bitmap)small_Image;
            int w2 = smallImage.Width;
            int h2 = smallImage.Height;
            Bitmap mergedImage = new Bitmap(w1, h1);
            for (int i = 0; i < w1; i++)
            {
                for (int j = 0; j < h1; j++)
                {
                    float ni = ((float)w2 / w1) * (i);
                    float nj = ((float)h2 / h1) * (j);
                    int i_small = (int)ni;
                    int j_small = (int)nj;
                    Color coverColor = smallImage.GetPixel(i_small, j_small);
                    Color oldColor = largeImage.GetPixel(i, j);
                    int alpha = (oldColor.A * coverColor.A) / 255;
                    int red = (oldColor.R * coverColor.R) / 255;
                    int green = (oldColor.G * coverColor.G) / 255;
                    int blue = (oldColor.B * coverColor.B) / 255;
                    mergedImage.SetPixel(i, j, Color.FromArgb(alpha, red, green, blue));
                }
            }
            Graphics gr = Graphics.FromImage(mergedImage);
            float scaleX = (float)mergedImage.Width / pictureBox1.Image.Width;
            float scaleY = (float)mergedImage.Height / pictureBox1.Image.Height;

            Pen blackPen = new Pen(Color.FromArgb(255, 0, 0, 0), 3);
            Rectangle r = new Rectangle(new Point((int)(SelectedPixel.X * scaleX), (int)(SelectedPixel.Y * scaleY)), new Size((int)scaleX, (int)scaleY));
            gr.DrawRectangle(blackPen, r);
            //Point[] myPts = { new Point(1, 1), new Point(40, 200), new Point(100, 300) };
            //gr.DrawPolygon(blackPen,myPts);

            return mergedImage;
        }

        private void btnExport_Click_1(object sender, EventArgs e)
        {
            double topCoordinates;
            double leftCoordinates;
            using (var reader = new StreamReader(@"C:\Users\Public\DSS\" + "last_selection_Top_Left.csv"))
            {
                var values = reader.ReadLine().Split(',');
                topCoordinates = Convert.ToDouble(values[0]) - 0.025;
                leftCoordinates = Convert.ToDouble(values[1]) + 0.025;
            }
            using (SaveFileDialog saveFileDialog = new SaveFileDialog() { Filter = @"PNG|*.png" })
            {
                if (saveFileDialog.ShowDialog() == DialogResult.OK)
                {
                    pictureBox1.Image.Save(saveFileDialog.FileName);
                    using (var tw = new StreamWriter(saveFileDialog.FileName.Replace("png", "pgw")))
                    {
                        tw.WriteLine("0.05");
                        tw.WriteLine("0.000000000");
                        tw.WriteLine("0.000000000");
                        tw.WriteLine("-0.05");
                        tw.WriteLine(leftCoordinates);
                        tw.WriteLine(topCoordinates);
                    }
                    using (var twn = new StreamWriter(saveFileDialog.FileName.Replace("png", "prj")))
                    {
                        twn.WriteLine("GEOGCS[\"GCS_WGS_1984\", DATUM[\"D_WGS84\", SPHEROID[\"WGS84\", 6378137, 298.257223563]], PRIMEM[\"Greenwich\", 0], UNIT[\"Degree\", 0.017453292519943295]]");

                    }
                }
            }
        }

        private void trans_ValueChanged(object sender, EventArgs e)
        {
            transValue = trans.Value * 255 / 10;

            List<Color> pixelColors = new List<Color>();

            Bitmap bmp = updateImage(DTW, TDS, Slope, ET, Sus, Existing, ids);
            pictureBox1.Image = bmp;
            pictureBox1.Parent = pictureBox2;
            pictureBox1.BackColor = Color.Transparent;
            pictureBox1.Location = new Point(0, 0);

        }

        private Bitmap updateImage(List<string> f1, List<string> f2, List<string> f3, List<string> f4, List<string> f5, List<string> f6, List<string> ids)
        {
            if (rbSatImg.Checked)
            {
                using (var bmpTemp = new Bitmap(@"C:\Users\Public\DSS\extracted_img_Sat.png"))
                {
                    pictureBox2.Image = new Bitmap(bmpTemp);
                }
            }

            if (rbStreetMap.Checked)
            {
                using (var bmpTemp = new Bitmap(@"C:\Users\Public\DSS\extracted_img_Street.png"))
                {
                    pictureBox2.Image = new Bitmap(bmpTemp);
                }
            }

            List<Color> pixelColors = new List<Color>();

            double r1 = Convert.ToDouble(value1.Text);
            double r2 = Convert.ToDouble(value2.Text);
            double r3 = Convert.ToDouble(value3.Text);
            double r4 = Convert.ToDouble(value4.Text);
            double r5 = Convert.ToDouble(value5.Text);
            double r6 = Convert.ToDouble(value6.Text);


            for (int i = 0; i < ids.Count; i++)
            {
                int index = Convert.ToInt32(ids[i]);
                double a1 = r1 * Convert.ToDouble(f1[index]);
                double a2 = r2 * Convert.ToDouble(f2[index]);
                double a3 = r3 * Convert.ToDouble(f3[index]);
                double a4 = r4 * Convert.ToDouble(f4[index]);
                double a5 = r5 * Convert.ToDouble(f5[index]);
                double a6 = r6 * Convert.ToDouble(f6[index]);

                pixelColors.Add(getColorFromValue(a1 + a2 + a3 + a4 + a5 + a6, transValue));
            }

            int width = (int)(pictureBox2.Image.Width / 37.282), height = (int)(pictureBox2.Image.Height / 37.282);

            Bitmap bmp = new Bitmap(width, height);

            int counter = 0;
            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    if (SelectedPixel.X == x && SelectedPixel.Y == y)
                    {
                        int index = Convert.ToInt32(ids[counter]);

                        txtProp1.Text = f1[index] + "";
                        txtProp2.Text = f2[index] + "";
                        txtProp3.Text = f3[index] + "";
                        txtProp4.Text = f4[index] + "";
                        txtProp5.Text = f5[index] + "";
                        txtProp6.Text = f6[index] + "";

                        double a1 = r1 * Convert.ToDouble(f1[index]);
                        double a2 = r2 * Convert.ToDouble(f2[index]);
                        double a3 = r3 * Convert.ToDouble(f3[index]);
                        double a4 = r4 * Convert.ToDouble(f4[index]);
                        double a5 = r5 * Convert.ToDouble(f5[index]);
                        double a6 = r6 * Convert.ToDouble(f6[index]);


                        txtProp7.Text = (int)(a1 + a2 + a3 + a4 + a5 + a6) + " %";
                        txtProp7.BackColor = getColorFromValue(a1 + a2 + a3 + a4 + a5 + a6, 255);
                    }
                    Color c = pixelColors[counter];
                    bmp.SetPixel(x, y, c);
                    counter++;
                }
            }


            if (r1 + r2 + r3 + r4 + r5 + r6 != 1)
            {
                lblError.Visible = true;
                value1.ForeColor = Color.Red;
                value2.ForeColor = Color.Red;
                value3.ForeColor = Color.Red;
                value4.ForeColor = Color.Red;
                value5.ForeColor = Color.Red;
                value6.ForeColor = Color.Red;
            }
            else
            {
                lblError.Visible = false;
                value1.ForeColor = Color.Black;
                value2.ForeColor = Color.Black;
                value3.ForeColor = Color.Black;
                value4.ForeColor = Color.Black;
                value5.ForeColor = Color.Black;
                value6.ForeColor = Color.Black;
            }

            pictureBox1.Image = bmp;
            pictureBox1.Parent = pictureBox2;
            pictureBox1.BackColor = Color.Transparent;

            return bmp;

        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            picColor1.Visible = true;
            picColor2.Visible = false;

            Bitmap bmp = updateImage(DTW, TDS, Slope, ET, Sus, Existing, ids);

            pictureBox1.Image = bmp;
            pictureBox1.Parent = pictureBox2;
            pictureBox1.BackColor = Color.Transparent;
            pictureBox1.Location = new Point(0, 0);
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            picColor1.Visible = false;
            picColor2.Visible = true;

            Bitmap bmp = updateImage(DTW, TDS, Slope, ET, Sus, Existing, ids);
            pictureBox1.Image = bmp;
            pictureBox1.Parent = pictureBox2;
            pictureBox1.BackColor = Color.Transparent;
            pictureBox1.Location = new Point(0, 0);
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox1.Text == "Custom")
            {
                slider1.Value = 2;
                slider2.Value = 2;
                slider3.Value = 3;
                slider4.Value = 3;
                slider5.Value = 5;
                slider6.Value = 5;

                updateImage(DTW, TDS, Slope, ET, Sus, Existing, ids);
            }


            if (comboBox1.Text == "Selection1")
            {
                slider1.Value = 3;
                slider2.Value = 3;
                slider3.Value = 2;
                slider4.Value = 2;
                slider5.Value = 5;
                slider6.Value = 5;

                updateImage(DTW, TDS, Slope, ET, Sus, Existing, ids);
            }


            if (comboBox1.Text == "Selection2")
            {
                slider1.Value = 5;
                slider2.Value = 5;
                slider3.Value = 2;
                slider4.Value = 2;
                slider5.Value = 3;
                slider6.Value = 3;

                updateImage(DTW, TDS, Slope, ET, Sus, Existing, ids);
            }


            if (comboBox1.Text == "Selection3")
            {
                slider1.Value = 3;
                slider2.Value = 3;
                slider3.Value = 5;
                slider4.Value = 5;
                slider5.Value = 2;
                slider6.Value = 2;

                updateImage(DTW, TDS, Slope, ET, Sus, Existing, ids);
            }


        }

        private void pictureBox1_MouseUp(object sender, MouseEventArgs e)
        {
            //Bitmap b = new Bitmap(pictureBox1.ClientSize.Width, pictureBox1.ClientSize.Height);
            Bitmap b = new Bitmap(pictureBox1.Image);
            float nx = ((float)b.Width / pictureBox1.ClientSize.Width) * (e.X);
            float ny = ((float)b.Height / pictureBox1.ClientSize.Height) * (e.Y);

            Color color = b.GetPixel((int)nx, (int)ny);
            SelectedPixel.X = (int)nx;
            SelectedPixel.Y = (int)ny;
            Bitmap bmp = updateImage(DTW, TDS, Slope, ET, Sus, Existing, ids);

            pictureBox1.Image = bmp;
            pictureBox1.Parent = pictureBox2;
            pictureBox1.BackColor = Color.Transparent;
            pictureBox1.Location = new Point(0, 0);
            propPanel.Visible = true;
        }

        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {
            if (SelectedPixel.X != -9 && SelectedPixel.Y != -9)
            {
                float scaleX = (float)pictureBox1.ClientSize.Width / pictureBox1.Image.Width;
                float scaleY = (float)pictureBox1.ClientSize.Height / pictureBox1.Image.Height;
                Pen blackPen = new Pen(Color.FromArgb(255, 0, 0, 0), 3);
                e.Graphics.DrawRectangle(blackPen, SelectedPixel.X * scaleX, SelectedPixel.Y * scaleY, scaleX, scaleY);
                int s = 5;
            }
        }


        private void rbSatImg_CheckedChanged(object sender, EventArgs e)
        {
            pictureBox2.Image = Image.FromFile(@"C:\Users\Public\DSS\extracted_img_Sat.png");
        }

        private void rbStreetMap_CheckedChanged(object sender, EventArgs e)
        {
            pictureBox2.Image = Image.FromFile(@"C:\Users\Public\DSS\extracted_img_Street.png");
        }

        private void slider1_ValueChanged(object sender, EventArgs e)
        {
            value1.Text = values[slider1.Value].ToString();

            Bitmap bmp = updateImage(DTW, TDS, Slope, ET, Sus, Existing, ids);
            pictureBox1.Image = bmp;
            pictureBox1.Parent = pictureBox2;
            pictureBox1.BackColor = Color.Transparent;
            pictureBox1.Location = new Point(0, 0);

        }

        private void slider2_ValueChanged(object sender, EventArgs e)
        {
            value2.Text = values[slider2.Value].ToString();
            Bitmap bmp = updateImage(DTW, TDS, Slope, ET, Sus, Existing, ids);
            pictureBox1.Image = bmp;
            pictureBox1.Parent = pictureBox2;
            pictureBox1.BackColor = Color.Transparent;
            pictureBox1.Location = new Point(0, 0);

        }

        private void slider3_ValueChanged(object sender, EventArgs e)
        {
            value3.Text = values[slider3.Value].ToString();

            Bitmap bmp = updateImage(DTW, TDS, Slope, ET, Sus, Existing, ids);
            pictureBox1.Image = bmp;
            pictureBox1.Parent = pictureBox2;
            pictureBox1.BackColor = Color.Transparent;
            pictureBox1.Location = new Point(0, 0);

        }

        private void slider4_ValueChanged(object sender, EventArgs e)
        {
            value4.Text = values[slider4.Value].ToString();

            Bitmap bmp = updateImage(DTW, TDS, Slope, ET, Sus, Existing, ids);
            pictureBox1.Image = bmp;
            pictureBox1.Parent = pictureBox2;
            pictureBox1.BackColor = Color.Transparent;
            pictureBox1.Location = new Point(0, 0);

        }

        private void slider5_ValueChanged(object sender, EventArgs e)
        {
            value5.Text = values[slider5.Value].ToString();

            Bitmap bmp = updateImage(DTW, TDS, Slope, ET, Sus, Existing, ids);
            pictureBox1.Image = bmp;
            pictureBox1.Parent = pictureBox2;
            pictureBox1.BackColor = Color.Transparent;
            pictureBox1.Location = new Point(0, 0);

        }

        private void slider6_ValueChanged(object sender, EventArgs e)
        {
            value6.Text = values[slider6.Value].ToString();

            Bitmap bmp = updateImage(DTW, TDS, Slope, ET, Sus, Existing, ids);
            pictureBox1.Image = bmp;
            pictureBox1.Parent = pictureBox2;
            pictureBox1.BackColor = Color.Transparent;
            pictureBox1.Location = new Point(0, 0);
        }

        private void btnCreateWord_Click(object sender, EventArgs e)
        {
            Word.Application app = new Word.Application();

            String fileName = Path.GetTempFileName();
            File.WriteAllBytes(fileName, Properties.Resources.Template_DSS);
            Word.Document doc = app.Documents.Open(fileName);

            app.ActiveDocument.Characters.Last.Select(); 
            app.Selection.Collapse();

            object control = 1;
            doc.SelectContentControlsByTag("a").get_Item(ref control).Range.Text = "AAAAA This text added to A";
            doc.SelectContentControlsByTag("b").get_Item(ref control).Range.Text = "BBBBB This text added to B";
            doc.SelectContentControlsByTag("c").get_Item(ref control).Range.Text = "CCCCC This text added to C";

            MergeTwoImages(pictureBox2.Image,pictureBox1.Image).Save(@"C:\Users\Public\DSS\development_areas.png");

            doc.SelectContentControlsByTag("pic1").get_Item(ref control).Range.InlineShapes.AddPicture(@"C:\Users\Public\DSS\development_areas.png");

            using (SaveFileDialog saveFileDialog = new SaveFileDialog() { Filter = @"Word Document |*.docx" })
            {
                if (saveFileDialog.ShowDialog() == DialogResult.OK)
                {
                    app.ActiveDocument.SaveAs2(saveFileDialog.FileName);
                    app.Quit();
                    File.Delete(fileName);
                }
            }
        }
    }
}
