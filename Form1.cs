﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NDSS
{
        public partial class Form1 : Form
    {
        private Button currentButton;
        private Form activeForm;
        public Form1()
        {
            InitializeComponent();
        }
        private Color selectThemeColor(int btnColorIndex)
        {
            return ColorTranslator.FromHtml(ThemeColor.ColorList[btnColorIndex]);
        }
        private void ActivateButton(object btnSender, Color color) 
        {
            if (btnSender != null)
            {
                if (currentButton != (Button)btnSender)
                {
                    DisableButton();
                    currentButton = (Button)btnSender;
                    currentButton.BackColor = color;
                    currentButton.ForeColor = Color.White;
                    currentButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                    panelTitleBar.BackColor = color;
                    panelLogo.BackColor = ThemeColor.ChangeColorBrightness(color,-0.3);
                }
            }
            pictureBoxLogos.Visible = true;
        }

        private void DisableButton()
        {
            foreach (Control previousBtn in panelMenu.Controls)
            {
                if (previousBtn.GetType() == typeof(Button))
                {
                    previousBtn.BackColor = Color.FromArgb(51, 51, 76);
                    previousBtn.ForeColor = Color.Gainsboro;
                    previousBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                }
            }
        }

        private void OpenChildForm(Form childForm,object btnSender,Color color)
        {
            if (activeForm != null)
            {
                activeForm.Close();
            }
            ActivateButton(btnSender,color);
            activeForm = childForm;
            childForm.TopLevel = false;
            childForm.FormBorderStyle = FormBorderStyle.None;
            childForm.Dock = DockStyle.Fill;
            this.DesktopPanel.Controls.Add(childForm);
            this.DesktopPanel.Tag = childForm;
            childForm.BringToFront();
            childForm.Show();
            lblTitle.Text = childForm.Text;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            OpenChildForm(new Forms.Form1(), sender,selectThemeColor(20));
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (!Directory.Exists(@"C:\Users\Public\DSS"))
            {
                MessageBox.Show("Please select area first");
            }
            else 
            {
                OpenChildForm(new Forms.Form2(), sender, selectThemeColor(11));

            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (!Directory.Exists(@"C:\Users\Public\DSS"))
            {
                MessageBox.Show("Please select area first");
            }
            else
            {
                OpenChildForm(new Forms.Form3(), sender, selectThemeColor(13));
            }

        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (!Directory.Exists(@"C:\Users\Public\DSS"))
            {
                MessageBox.Show("Please select area first");
            }
            else
            {
                OpenChildForm(new Forms.Form4(), sender, selectThemeColor(17));
            }

        }

        private void button5_Click(object sender, EventArgs e)
        {
            ActivateButton(sender, selectThemeColor(16));

        }

        private void button6_Click(object sender, EventArgs e)
        {
            ActivateButton(sender, selectThemeColor(18));

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void pictureBoxLogos_Click(object sender, EventArgs e)
        {

        }
    }
}
