﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NDSS.Forms
{
    public partial class Form2 : Form
    {
        private Button currentButton;
        Color btnsColor;
        private Random random;
        private int tempIndex;
        List<string>[] inputData;
        List<string> ids;
        List<string> DTW;
        List<string> TDS;
        List<string> Slope;
        List<string> ET;
        List<string> Sus;
        List<string> Existing;
        List<string> currentData;
        double currentMax = 250;
        double currentMin = 0;
        int transValue = 127;



        public Form2()
        {
            InitializeComponent();
            panelMenu2.BackColor = selectThemeColor(11);
            btnsColor = selectThemeColor(6);
            btnExport.BackColor = ColorTranslator.FromHtml(ThemeColor.ColorList[11]);


            inputData = readMultiColumn(NDSS.Properties.Resources.input_realData2,6);
            ids = idReader(@"C:\Users\Public\DSS\extracted_ids.csv");


            DTW = inputData[0];
            TDS = inputData[1];
            Slope = inputData[2];
            ET = inputData[3];
            Sus = inputData[4];
            Existing = inputData[5];

            using (var bmpTemp = new Bitmap(@"C:\Users\Public\DSS\extracted_img_Sat.png"))
            {
                double inputWidth = bmpTemp.Width;
                double inputHeight = bmpTemp.Height;

                if (inputWidth > inputHeight )
                {
                    pictureBox1.Height = (int) ((inputHeight / inputWidth) * pictureBox1.Width);
                    pictureBox2.Height = pictureBox1.Height;

                    Point newLocation = new Point(pictureBox1.Location.X, pictureBox1.Location.Y + 250 - (int)(pictureBox1.Height / 2));

                    pictureBox1.Location = newLocation;
                    pictureBox2.Location = newLocation;
                }

                if (inputHeight > inputWidth)
                {
                    pictureBox1.Width = (int)((inputWidth / inputHeight) * pictureBox1.Height);
                    pictureBox2.Width = pictureBox1.Width;

                    Point newLocation = new Point(pictureBox1.Location.X + 250 - (int)(pictureBox1.Width / 2), pictureBox1.Location.Y );

                    pictureBox1.Location = newLocation;
                    pictureBox2.Location = newLocation;
                }

            }



            ActivateButton(button1, btnsColor);
            currentData = DTW;

            lblRange1.Text = "50";
            lblRange2.Text = "100";
            lblRange3.Text = "150";
            lblRange4.Text = "200";

            updateImage(currentData, ids);

            pixelBox1.Parent = pictureBox1;
            pixelBox1.Location = new Point(440, 0);
        }

        public List<string>[] readMultiColumn(string myData,int numberOfColumns)
        {
            using (var reader = new StringReader(myData))
            {
                List<string>[] totalData = new List<string>[numberOfColumns];

                for (int i = 0; i < numberOfColumns; i++)
                {
                    totalData[i] = new List<string>();
                }

                String line;
                while ((line = reader.ReadLine()) != null)
                {
                    var values = line.Split(',');
                    if (line != "")
                    {
                        for (int i = 0; i < numberOfColumns; i++)
                        {
                            totalData[i].Add(values[i]);
                        }
                    }

                }
                return totalData;
            }
        }


        public List<string> idReader(string filePath)
        {
            if (!Directory.Exists(@"C:\Users\Public\DSS"))
            {
                MessageBox.Show("Please select area first");
                List<string> emptyString = new List<string>();
                for (int i = 0;i<400;i++) {
                    emptyString.Add("43600");
                }
                return emptyString;
            }
            using (var reader = new StreamReader(filePath))
            {
                List<string> idValues = new List<string>();

                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();

                    var values = line.Split(',');
                    if (line != "")
                    {
                        idValues.Add(values[0]);
                    }

                }
                return idValues;
            }
        }

        private void updateImage(List<string> data, List<string> ids)
        {
            if (rbSatImg.Checked)
            {
                using (var bmpTemp = new Bitmap(@"C:\Users\Public\DSS\extracted_img_Sat.png"))
                {
                    pictureBox2.Image = new Bitmap(bmpTemp);
                }
            }

            if (rbStreetMap.Checked)
            {
                using (var bmpTemp = new Bitmap(@"C:\Users\Public\DSS\extracted_img_Street.png"))
                {
                    pictureBox2.Image = new Bitmap(bmpTemp);
                }
            }


            List<Color> pixelColors = new List<Color>();
            for (int i = 0; i < ids.Count; i++)
            {
                int index = Convert.ToInt32(ids[i]);
                double value = Convert.ToDouble(data[index]);
                pixelColors.Add(getColorFromValue(value, transValue));
            }

            int width = (int) (pictureBox2.Image.Width/ 37.282), height = (int)(pictureBox2.Image.Height / 37.282);

            Bitmap bmp = new Bitmap(width, height);
            int counter = 0;
            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                   Color c = pixelColors[counter];
                   bmp.SetPixel(x, y, c);
                   counter++;
                }
            }

            pictureBox1.Image = bmp;



            pictureBox1.Parent = pictureBox2;
            pictureBox1.BackColor = Color.Transparent;
            pictureBox1.Location = new Point(0, 0);
        }

        private Color getColorFromValue(double value, int trans)
        {
            double diff = currentMax - currentMin;
            double step = diff / 5;

            lblCrntMax.Text = currentMax + "";
            lblCrntMin.Text = currentMin + "";

            if (radioButton1.Checked)
            {
                if (value > Convert.ToDouble(lblRange4.Text))
                {
                    return Color.FromArgb(trans, 0, 109, 44);
                }
                if (value > Convert.ToDouble(lblRange3.Text))
                {
                    return Color.FromArgb(trans, 44, 162, 95);
                }
                if (value > Convert.ToDouble(lblRange2.Text))
                {
                    return Color.FromArgb(trans, 102, 194, 164);
                }
                if (value > Convert.ToDouble(lblRange1.Text))
                {
                    return Color.FromArgb(trans, 178, 226, 226);
                }
                if (value >= currentMin)
                {
                    return Color.FromArgb(trans, 237, 248, 251);
                }
            }

            if (radioButton2.Checked)
            {
                if (value > Convert.ToDouble(lblRange4.Text))
                {
                    return Color.FromArgb(trans, 215, 25, 28);
                }
                if (value > Convert.ToDouble(lblRange3.Text))
                {
                    return Color.FromArgb(trans, 253, 174, 97);
                }
                if (value > Convert.ToDouble(lblRange2.Text))
                {
                    return Color.FromArgb(trans, 255, 255, 191);
                }
                if (value > Convert.ToDouble(lblRange1.Text))
                {
                    return Color.FromArgb(trans, 171, 217, 233);

                }
                if (value >= currentMin)
                {
                    return Color.FromArgb(trans, 44, 123, 182);

                }
            }

            return Color.FromArgb(trans, 255, 255, 255);
        }

        private Color selectThemeColor(int btnColorIndex)
        {
            return ColorTranslator.FromHtml(ThemeColor.ColorList[btnColorIndex]);
        }

        private void ActivateButton(object btnSender,Color color)
        {
            if (btnSender != null)
            {
                if (currentButton != (Button)btnSender)
                {
                    DisableButton();
                    currentButton = (Button)btnSender;
                    currentButton.BackColor = color;
                    currentButton.ForeColor = Color.White;
                    currentButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                }
            }
        }

        private void DisableButton()
        {
            foreach (Control previousBtn in panelMenu2.Controls)
            {
                if (previousBtn.GetType() == typeof(Button))
                {
                    previousBtn.BackColor = selectThemeColor(11);
                    previousBtn.ForeColor = Color.Gainsboro;
                    previousBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                }
            }
        }






        private void button1_Click(object sender, EventArgs e)
        {
            ActivateButton(sender, btnsColor);
            currentData = DTW;
            currentMax = 250;
            currentMin = 0;

            lblRange1.Text = "50";
            lblRange2.Text = "100";
            lblRange3.Text = "150";
            lblRange4.Text = "200";

            updateImage(currentData, ids);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ActivateButton(sender, btnsColor);
            currentData = TDS;
            currentMax = 10000;
            currentMin = 100;

            lblRange1.Text = "1000";
            lblRange2.Text = "2000";
            lblRange3.Text = "3000";
            lblRange4.Text = "4000";

            updateImage(currentData, ids);


        }

        private void button3_Click(object sender, EventArgs e)
        {
            ActivateButton(sender, btnsColor);
            currentData = Slope;
            currentMax = 25;
            currentMin = 0;

            lblRange1.Text = "5";
            lblRange2.Text = "10";
            lblRange3.Text = "15";
            lblRange4.Text = "20";

            updateImage(currentData, ids);

        }

        private void button4_Click(object sender, EventArgs e)
        {
            ActivateButton(sender, btnsColor);
            currentData = ET;
            currentMax = 13.1;
            currentMin = 6.6;

            lblRange1.Text = "7.9";
            lblRange2.Text = "9.2";
            lblRange3.Text = "10.3";
            lblRange4.Text = "11.8";

            updateImage(currentData, ids);

        }

        private void button5_Click(object sender, EventArgs e)
        {
            ActivateButton(sender, btnsColor);
            currentData = Sus;
            currentMax = 15;
            currentMin = 5;

            lblRange1.Text = "7";
            lblRange2.Text = "9";
            lblRange3.Text = "11";
            lblRange4.Text = "13";

            updateImage(currentData, ids);

        }

        private void button6_Click(object sender, EventArgs e)
        {
            ActivateButton(sender, btnsColor);
            currentData = Existing;
            currentMax = 1;
            currentMin = 0;

            lblRange1.Text = "0.2";
            lblRange2.Text = "0.4";
            lblRange3.Text = "0.6";
            lblRange4.Text = "0.8";

            updateImage(currentData, ids);

        }

        private void panelMenu2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void radioButton1_CheckedChanged_1(object sender, EventArgs e)
        {
            picColor1.Visible = true; 
            picColor2.Visible = false;
            updateImage(currentData, ids);
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            picColor1.Visible = false;
            picColor2.Visible = true;
            updateImage(currentData, ids);
        }

        private void trans_ValueChanged(object sender, EventArgs e)
        {
            transValue = trans.Value * 255 / 10;
            List<Color> pixelColors = new List<Color>();
            updateImage(currentData, ids);
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            double topCoordinates;
            double leftCoordinates;
            using (var reader = new StreamReader(@"C:\Users\Public\DSS\" + "last_selection_Top_Left.csv"))
            {
                var values = reader.ReadLine().Split(',');
                topCoordinates = Convert.ToDouble(values[0]) - 0.025;
                leftCoordinates = Convert.ToDouble(values[1]) + 0.025;
            }

                using (SaveFileDialog saveFileDialog = new SaveFileDialog() { Filter = @"PNG|*.png" })
            {
                if (saveFileDialog.ShowDialog() == DialogResult.OK)
                {
                    pictureBox1.Image.Save(saveFileDialog.FileName);
                    using (var tw = new StreamWriter(saveFileDialog.FileName.Replace("png", "pgw")))
                    {
                        tw.WriteLine("0.05");
                        tw.WriteLine("0.000000000");
                        tw.WriteLine("0.000000000");
                        tw.WriteLine("-0.05");
                        tw.WriteLine(leftCoordinates);
                        tw.WriteLine(topCoordinates);
                    }
                    using (var twn = new StreamWriter(saveFileDialog.FileName.Replace("png", "prj")))
                    {
                        twn.WriteLine("GEOGCS[\"GCS_WGS_1984\", DATUM[\"D_WGS84\", SPHEROID[\"WGS84\", 6378137, 298.257223563]], PRIMEM[\"Greenwich\", 0], UNIT[\"Degree\", 0.017453292519943295]]");

                    }
                }
            }
        }

        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void rbSatImg_CheckedChanged(object sender, EventArgs e)
        {
            using (var bmpTemp = new Bitmap(@"C:\Users\Public\DSS\extracted_img_Sat.png"))
            {
                pictureBox2.Image = new Bitmap(bmpTemp);
            }
        }

        private void rbStreetMap_CheckedChanged(object sender, EventArgs e)
        {
            using (var bmpTemp = new Bitmap(@"C:\Users\Public\DSS\extracted_img_Street.png"))
            {
                pictureBox2.Image = new Bitmap(bmpTemp);
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }
    }
}
