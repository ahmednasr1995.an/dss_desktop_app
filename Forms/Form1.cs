﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NDSS.Forms
{
    public partial class Form1 : Form
    {
        private Button currentButton;
        Color btnsColor;
        Point[] myPoints;
        Point[] polygonPoints;
        List<string>[] ID_xy;
        List<string> idAll;
        List<string> xAll;
        List<string> yAll;


        int xMD = -9;
        int yMD = -9;

        int xMU = -9;
        int yMU = -9;


        public Form1()
        {
            InitializeComponent();
            panelMenu2.BackColor = selectThemeColor(20);
            btnsColor = selectThemeColor(6);

            ActivateButton(button1, selectThemeColor(6));

            ID_xy = readMultiColumn(NDSS.Properties.Resources.ID_xy, 3);

            idAll = ID_xy[0];
            xAll = ID_xy[1];
            yAll = ID_xy[2];

            if (File.Exists(@"C:\Users\Public\DSS\" + "last_selection.csv"))
            {
                String filepath = @"C:\Users\Public\DSS\" + "last_selection.csv";
                if (filepath != "")
                {
                    List<double> areaValues = getAreaValues(filepath);
                    myPoints = getPolygonVerticesFromMaxMin(areaValues);
                    pictureBox1.Refresh();
                    txtLeft.Text = areaValues[0] + "";
                    txtRight.Text = areaValues[1] + "";
                    txtBottom.Text = areaValues[2] + "";
                    txtTop.Text = areaValues[3] + "";

                }

            }
            pixelBox1.Parent = pictureBox1;
            pixelBox1.Location = new Point(707, 0);
        }

        private Color selectThemeColor(int btnColorIndex)
        {
            return ColorTranslator.FromHtml(ThemeColor.ColorList[btnColorIndex]);
        }

        private void ActivateButton(object btnSender, Color color)
        {
            if (btnSender != null)
            {
                if (currentButton != (Button)btnSender)
                {
                    DisableButton();
                    currentButton = (Button)btnSender;
                    currentButton.BackColor = color;
                    currentButton.ForeColor = Color.White;
                    currentButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                    if (btnSender!= button3) 
                    {
                        panelMaxMin.Visible = false;
                    }
                    if (btnSender != button4)
                    {
                        panelPoint.Visible = false;
                    }
                }
            }
        }

        private void DisableButton()
        {
            foreach (Control previousBtn in panelMenu2.Controls)
            {
                if (previousBtn.GetType() == typeof(Button))
                {
                    previousBtn.BackColor = selectThemeColor(20);
                    previousBtn.ForeColor = Color.Gainsboro;
                    previousBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                }
            }
        }

        public List<string>[] readMultiColumn(string myData, int numberOfColumns)
        {
            using (var reader = new StringReader(myData))
            {
                List<string>[] totalData = new List<string>[numberOfColumns];

                for (int i = 0; i < numberOfColumns; i++)
                {
                    totalData[i] = new List<string>();
                }

                String line;
                while ((line = reader.ReadLine()) != null)
                {
                    var values = line.Split(',');
                    if (line != "")
                    {
                        for (int i = 0; i < numberOfColumns; i++)
                        {
                            totalData[i].Add(values[i]);
                        }
                    }

                }
                return totalData;
            }
        }

        private String ReadInputFile() 
        {
            using (OpenFileDialog openFileDialog = new OpenFileDialog() { Filter = @"CSV|*.csv" })
            {
                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    String filepath = openFileDialog.FileName;
                    return filepath;
                }
                return "";
            }

        }

        private List<double> getAreaValues(String filePath)
        {            
                using (var reader = new StreamReader(filePath))
                {
                    List<double> areaValues = new List<double>();

                    reader.ReadLine();
                    var line = reader.ReadLine();

                    var values = line.Split(',');
                    if (line != "")
                    {
                        areaValues.Add(Convert.ToDouble(values[0]));
                        areaValues.Add(Convert.ToDouble(values[1]));
                        areaValues.Add(Convert.ToDouble(values[2]));
                        areaValues.Add(Convert.ToDouble(values[3]));
                    }
                    return areaValues;
                }
        }

        private Point[] getPolygonVerticesFromMaxMin(List<double> areaValues)
        {
            Point[] myPoints = new Point[4];

            double x1 = (areaValues[0] - 25) * 68;
            double y1 = (31.5 - areaValues[2]) * 68;
            myPoints[0] = new Point((int)x1, (int)y1);

            double x2 = (areaValues[0] - 25) * 68;
            double y2 = (31.5 - areaValues[3]) * 68;
            myPoints[1] = new Point((int)x2, (int)y2);

            double x3 = (areaValues[1] - 25) * 68;
            double y3 = (31.5 - areaValues[3]) * 68;
            myPoints[2] = new Point((int)x3, (int)y3);


            double x4 = (areaValues[1] - 25) * 68;
            double y4 = (31.5 - areaValues[2]) * 68;
            myPoints[3] = new Point((int)x4, (int)y4);

            return myPoints;
        }

        private PointF[] getPolygonVerticesFromMaxMinF(List<double> areaValues)
        {
            PointF[] myPoints = new PointF[4];

            double x1 = (areaValues[0] - 25) * 68;
            double y1 = (31.5 - areaValues[2]) * 68;
            myPoints[0] = new PointF((int)x1, (int)y1);

            double x2 = (areaValues[0] - 25) * 68;
            double y2 = (31.5 - areaValues[3]) * 68;
            myPoints[1] = new Point((int)x2, (int)y2);

            double x3 = (areaValues[1] - 25) * 68;
            double y3 = (31.5 - areaValues[3]) * 68;
            myPoints[2] = new Point((int)x3, (int)y3);


            double x4 = (areaValues[1] - 25) * 68;
            double y4 = (31.5 - areaValues[2]) * 68;
            myPoints[3] = new Point((int)x4, (int)y4);

            return myPoints;
        }

        private void getInsideDrawnPolygon(List<double> areaValues)
        {
            double xMinLocal =Math.Round((areaValues[0] - 25) * 20);
            double xMaxLocal = Math.Round((areaValues[1] - 25) * 20);
            double yMaxLocal = Math.Round((31.5 - areaValues[2]) * 20);
            double yMinLocal = Math.Round((31.5 - areaValues[3]) * 20);

            string dir = @"C:\Users\Public\DSS"; // If directory does not exist, create it.
            if (!Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }
            using (var tw = new StreamWriter(@"C:\Users\Public\DSS\extracted_ids.csv"))
            {
                for (int i = 1; i < idAll.Count ; i++)
                {
                    int x = Convert.ToInt32(xAll[i]);
                    int y = Convert.ToInt32(yAll[i]);

                    if (x > xMinLocal && x <= xMaxLocal && y > yMinLocal && y <= yMaxLocal)
                    {
                        tw.WriteLine(idAll[i]);
                    }
                }

            }
        }

        private Bitmap[] cropImage(Bitmap sourceBitmap1, Bitmap sourceBitmap2, List<double> areaValues) 
        {
            int xMinLocal1 = (int)((areaValues[0] - 25) * (sourceBitmap1.Width / 11.5));
            int xMaxLocal1 = (int)((areaValues[1] - 25) * (sourceBitmap1.Width / 11.5));
            int yMaxLocal1 = (int)((31.5 - areaValues[2]) * (sourceBitmap1.Height / 9.5));
            int yMinLocal1 = (int)((31.5 - areaValues[3]) * (sourceBitmap1.Height / 9.5));

            int xMinLocal2 = (int)((areaValues[0] - 25) * (sourceBitmap2.Width / 11.5));
            int xMaxLocal2 = (int)((areaValues[1] - 25) * (sourceBitmap2.Width / 11.5));
            int yMaxLocal2 = (int)((31.5 - areaValues[2]) * (sourceBitmap2.Height / 9.5));
            int yMinLocal2 = (int)((31.5 - areaValues[3]) * (sourceBitmap2.Height / 9.5));

            Rectangle cropRect1 = new Rectangle(xMinLocal1,yMinLocal1, xMaxLocal1 - xMinLocal1, yMaxLocal1 - yMinLocal1);
            Rectangle targetRect1 = new Rectangle(0, 0, xMaxLocal1 - xMinLocal1, yMaxLocal1 - yMinLocal1);

            Rectangle cropRect2 = new Rectangle(xMinLocal2, yMinLocal2, xMaxLocal2 - xMinLocal2, yMaxLocal2 - yMinLocal2);
            Rectangle targetRect2 = new Rectangle(0, 0, xMaxLocal2 - xMinLocal2, yMaxLocal2 - yMinLocal2);

            Bitmap smallImg11 = new Bitmap(xMaxLocal1 - xMinLocal1+1, yMaxLocal1 - yMinLocal1+1);
            Bitmap smallImg22 = new Bitmap(xMaxLocal2 - xMinLocal2+1, yMaxLocal2 - yMinLocal2+1);


            CopyRegionIntoImage(sourceBitmap1,cropRect1,ref smallImg11,targetRect1);
            CopyRegionIntoImage(sourceBitmap2, cropRect2, ref smallImg22, targetRect2);

            Bitmap[] bitmaps = { smallImg11, smallImg22 };

            return bitmaps;
        }

        public static void CopyRegionIntoImage(Bitmap srcBitmap, Rectangle srcRegion, ref Bitmap destBitmap, Rectangle destRegion)
        {
            using (Graphics grD = Graphics.FromImage(destBitmap))
            {
                grD.DrawImage(srcBitmap, destRegion, srcRegion, GraphicsUnit.Pixel);
            }
        }


        private void button1_Click(object sender, EventArgs e)
        {
            ActivateButton(sender, btnsColor);
            polygonPoints = null;
            //MessageBox.Show("This function is under development");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            ActivateButton(sender, btnsColor);

            List<double> areaValues = new List<double>();

            double xMin;
            double xMax;
            double yMin;
            double yMax;

            String filepath =  ReadInputFile();

            if (File.Exists(@"C:\Users\Public\DSS\" + "last_selection.csv"))
            {
                File.Delete(@"C:\Users\Public\DSS\" + "last_selection.csv");
            }

            if (!Directory.Exists(@"C:\Users\Public\DSS\"))
            {
                Directory.CreateDirectory(@"C:\Users\Public\DSS\");
            }

            if (filepath != "") 
            {
                areaValues = getAreaValues(filepath);
                xMin = areaValues[0];
                xMax = areaValues[1];
                yMin = areaValues[2];
                yMax = areaValues[3];

                double x = (xMax + xMin) / 2;
                double y = (yMax + yMin) / 2;

                bool checkEntries = (x <= 36.5 && x >= 25 && y >= 22 && y <= 31.5);
                if (checkEntries)
                {
                    areaValues = getAreaValuesFromPoint(x, y);

                    myPoints = getPolygonVerticesFromMaxMin(areaValues);
                    pictureBox1.Refresh();
                    getInsideDrawnPolygon(areaValues);

                    Bitmap[] croppedImages = cropImage(NDSS.Properties.Resources.Egypt_Basemap_High_quality, NDSS.Properties.Resources.street, areaValues);
                    croppedImages[0].Save(@"C:\Users\Public\DSS\extracted_img_Sat.png");
                    croppedImages[1].Save(@"C:\Users\Public\DSS\extracted_img_Street.png");

                    using (var tw = new StreamWriter(@"C:\Users\Public\DSS\" + "last_selection.csv"))
                    {
                        tw.WriteLine();
                        String myText = areaValues[0] + "," + areaValues[1] + "," + areaValues[2] + "," + areaValues[3];
                        tw.WriteLine(myText);
                    }

                    using (var tw = new StreamWriter(@"C:\Users\Public\DSS\" + "last_selection_Top_Left.csv"))
                    {
                        String coordinates = areaValues[3] + "," + areaValues[0];
                        tw.WriteLine(coordinates);
                    }
                }

                List<double> inputPolygon = getAreaValues(filepath);
                if ((inputPolygon[1] - inputPolygon[0]) > 1 || (inputPolygon[3] - inputPolygon[2]) > 1)
                {
                    MessageBox.Show(" Selected area greater than 1 degree by 1 degree,\n Centered 1 degree by 1 degree area will be shown");
                }
                else
                {
                    if (File.Exists(@"C:\Users\Public\DSS\" + "last_selection.csv")) 
                    {
                        File.Delete(@"C:\Users\Public\DSS\" + "last_selection.csv");
                    }
                    File.Copy(filepath, @"C:\Users\Public\DSS\" + "last_selection.csv");
                    polygonPoints = getPolygonVerticesFromMaxMin(inputPolygon);
                    pictureBox1.Refresh();
                }
            }
        }

        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {
            Pen blackPen = new Pen(Color.FromArgb(255, 0, 0, 0), 3);
            if (myPoints != null)
            {
                e.Graphics.DrawPolygon(blackPen, myPoints);
            }
            if (polygonPoints != null)
            {
                e.Graphics.DrawPolygon(blackPen, polygonPoints);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            ActivateButton(sender, btnsColor);
            panelMaxMin.Visible = true;
        }

        private void button4_Click_1(object sender, EventArgs e)
        {
            ActivateButton(sender, btnsColor);
            panelPoint.Visible = true;

        }

        private void txtTop_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtRight.Select();
            }
        }

        private void txtRight_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtBottom.Select();
            }
        }

        private void txtBottom_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtLeft.Select();
            }
        }

        private void txtLeft_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtTop.Select();
            }
        }

        private void txtX_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtY.Select();
            }
        }

        private void txtY_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                txtX.Select();
            }
        }

        private void insertMaxMin_Click(object sender, EventArgs e)
        {
            double xMin;
            double xMax;
            double yMin;
            double yMax;

            List<double> inputPolygon = new List<double>();

            bool checkEntriesIsDouble = Double.TryParse(txtLeft.Text, out xMin) && Double.TryParse(txtRight.Text, out xMax)
                && Double.TryParse(txtBottom.Text, out yMin) && Double.TryParse(txtTop.Text, out yMax);
            if (checkEntriesIsDouble)
            {
                xMin = Math.Round(Convert.ToDouble(txtLeft.Text) * 20) / 20;
                xMax = Math.Round(Convert.ToDouble(txtRight.Text) * 20) / 20;
                yMin = Math.Round(Convert.ToDouble(txtBottom.Text) * 20) / 20;
                yMax = Math.Round(Convert.ToDouble(txtTop.Text) * 20) / 20;


                if (xMax > xMin && yMax > yMin)
                {
                    inputPolygon.Add(xMin);
                    inputPolygon.Add(xMax);
                    inputPolygon.Add(yMin);
                    inputPolygon.Add(yMax);

                    double x = (xMax + xMin) / 2;
                    double y = (yMax + yMin) / 2;

                    bool checkEntries = (x <= 36.5 && x >= 25 && y >= 22 && y <= 31.5);
                    if (checkEntries)
                    {
                        List<double> areaValues = getAreaValuesFromPoint(x, y);

                        //myPoints = getPolygonVerticesFromMaxMin(areaValues);
                        //pictureBox1.Refresh();
                        //getInsideDrawnPolygon(areaValues); 

                        myPoints = polygonPoints;
                        pictureBox1.Refresh();
                        getInsideDrawnPolygon(inputPolygon);

                        Bitmap[] croppedImages = cropImage(NDSS.Properties.Resources.Egypt_Basemap_High_quality, NDSS.Properties.Resources.street, inputPolygon);
                        croppedImages[0].Save(@"C:\Users\Public\DSS\extracted_img_Sat.png");
                        croppedImages[1].Save(@"C:\Users\Public\DSS\extracted_img_Street.png");

                        using (var tw = new StreamWriter(@"C:\Users\Public\DSS\" + "last_selection.csv"))
                        {
                            tw.WriteLine();
                            String myText = inputPolygon[0] + "," + inputPolygon[1] + "," + inputPolygon[2] + "," + inputPolygon[3];
                            tw.WriteLine(myText);
                        }

                        using (var tw = new StreamWriter(@"C:\Users\Public\DSS\" + "last_selection_Top_Left.csv"))
                        {
                            String coordinates = inputPolygon[3] + "," + inputPolygon[0];
                            tw.WriteLine(coordinates);
                        }
                    }

                    myPoints = getPolygonVerticesFromMaxMin(inputPolygon);
                    pictureBox1.Refresh();
                }
                else
                {
                    MessageBox.Show("Please insert valid Geographic decimal degree values");
                }
            }
            else
            {
                MessageBox.Show("Please insert valid Geographic decimal degree values");
            }


        }

        private List<double> getAreaValuesFromPoint(double x, double y)
        {
            List<double> areaValues = new List<double>();

            double Top = y + 0.5;
            double Bottom = y - 0.5;
            double Left = x - 0.5;
            double Right = x + 0.5;

            if (Top > 31.5)
            {
                Top = 31.5;
                Bottom = 30.5;
            }

            if (Bottom < 22)
            {
                Top = 23;
                Bottom = 22;
            }

            if (Left < 25)
            {
                Left = 25;
                Right = 26;
            }

            if (Right > 36.5)
            {
                Left = 35.5;
                Right = 36.5;
            }

            areaValues.Add(Left);
            areaValues.Add(Right);
            areaValues.Add(Bottom);
            areaValues.Add(Top);

            return areaValues;

        }

        private void pictureBox1_MouseUp(object sender, MouseEventArgs e)
        {
            xMU = e.X;
            yMU = e.Y;

            if (button1.BackColor != selectThemeColor(20) && xMD == xMU && yMD == yMU)
            {
                double x = Math.Round((25 + (e.X * 11.5 / 779))*20)/20;
                double y = Math.Round((31.5 - (e.Y * 9.5 / 643)) * 20) / 20;

                List<double> areaValues = getAreaValuesFromPoint(x, y);

                myPoints = getPolygonVerticesFromMaxMin(areaValues);
                pictureBox1.Refresh();
                getInsideDrawnPolygon(areaValues);

                Bitmap[] croppedImages = cropImage(NDSS.Properties.Resources.Egypt_Basemap_High_quality, NDSS.Properties.Resources.street, areaValues);
                croppedImages[0].Save(@"C:\Users\Public\DSS\extracted_img_Sat.png");
                croppedImages[1].Save(@"C:\Users\Public\DSS\extracted_img_Street.png");

                txtLeft.Text = areaValues[0] + "";
                txtRight.Text = areaValues[1] + "";
                txtBottom.Text = areaValues[2] + "";
                txtTop.Text = areaValues[3] + "";

                using (var tw = new StreamWriter(@"C:\Users\Public\DSS\" + "last_selection.csv"))
                {
                    tw.WriteLine();
                    String myText = areaValues[0] + "," + areaValues[1] + "," + areaValues[2] + "," + areaValues[3];
                    tw.WriteLine(myText);
                }

                using (var tw = new StreamWriter(@"C:\Users\Public\DSS\" + "last_selection_Top_Left.csv"))
                {
                    String coordinates = areaValues[3] + "," + areaValues[0];
                    tw.WriteLine(coordinates);
                }
            }
            else
            {
                if (button1.BackColor != selectThemeColor(20))
                {
                    double xMin = Math.Round((25 + (Math.Min(xMU, xMD) * 11.5 / 782))*20)/20;
                    double xMax = Math.Round((25 + (Math.Max(xMU, xMD) * 11.5 / 782)) * 20) / 20;
                    double yMin = Math.Round((31.5 - (Math.Max(yMU, yMD) * 9.5 / 646))*20)/ 20;
                    double yMax = Math.Round((31.5 - (Math.Min(yMU, yMD) * 9.5 / 646))*20)/ 20;

                    List<double> inputPolygon = new List<double>();

                    inputPolygon.Add(xMin);
                    inputPolygon.Add(xMax);
                    inputPolygon.Add(yMin);
                    inputPolygon.Add(yMax);

                    txtLeft.Text = inputPolygon[0] + "";
                    txtRight.Text = inputPolygon[1] + "";
                    txtBottom.Text = inputPolygon[2] + "";
                    txtTop.Text = inputPolygon[3] + "";


                    myPoints = polygonPoints;
                    pictureBox1.Refresh();
                    getInsideDrawnPolygon(inputPolygon);

                    myPoints = getPolygonVerticesFromMaxMin(inputPolygon);
                    pictureBox1.Refresh();

                    Bitmap[] croppedImages = cropImage(NDSS.Properties.Resources.Egypt_Basemap_High_quality, NDSS.Properties.Resources.street, inputPolygon);
                    croppedImages[0].Save(@"C:\Users\Public\DSS\extracted_img_Sat.png");
                    croppedImages[1].Save(@"C:\Users\Public\DSS\extracted_img_Street.png");

                    using (var tw = new StreamWriter(@"C:\Users\Public\DSS\" + "last_selection.csv"))
                    {
                        tw.WriteLine();
                        String myText = inputPolygon[0] + "," + inputPolygon[1] + "," + inputPolygon[2] + "," + inputPolygon[3];
                        tw.WriteLine(myText);
                    }

                    using (var tw = new StreamWriter(@"C:\Users\Public\DSS\" + "last_selection_Top_Left.csv"))
                    {
                        String coordinates = inputPolygon[3] + "," + inputPolygon[0];
                        tw.WriteLine(coordinates);
                    }
                }
            }

        }

        private void insertXY_Click(object sender, EventArgs e)
        {
            polygonPoints = null;

            double x;
            double y;

            bool checkEntries = Double.TryParse(txtX.Text, out x) && Double.TryParse(txtY.Text, out y) && x <= 36.5 && x >= 25 && y >= 22 && y <= 31.5 ;
            if (checkEntries)
            {
                x = Convert.ToDouble(txtX.Text);
                y = Convert.ToDouble(txtY.Text);

                List<double> areaValues = getAreaValuesFromPoint(x, y);

                myPoints = getPolygonVerticesFromMaxMin(areaValues);
                pictureBox1.Refresh();
                getInsideDrawnPolygon(areaValues);

                Bitmap[] croppedImages = cropImage(NDSS.Properties.Resources.Egypt_Basemap_High_quality, NDSS.Properties.Resources.street, areaValues);
                croppedImages[0].Save(@"C:\Users\Public\DSS\extracted_img_Sat.png");
                croppedImages[1].Save(@"C:\Users\Public\DSS\extracted_img_Street.png");

                using (var tw = new StreamWriter(@"C:\Users\Public\DSS\" + "last_selection.csv"))
                {
                    tw.WriteLine();
                    String myText = areaValues[0] + "," + areaValues[1] + "," + areaValues[2] + "," + areaValues[3];
                    tw.WriteLine(myText);
                }

                using (var tw = new StreamWriter(@"C:\Users\Public\DSS\" + "last_selection_Top_Left.csv"))
                {
                    String coordinates = areaValues[3] + "," + areaValues[0];
                    tw.WriteLine(coordinates);
                }
            }
            else {
                MessageBox.Show("Invalid Entries");
            }
        }

        private void pictureBox1_MouseMove(object sender, MouseEventArgs e)
        {
            coordinatePanel.Visible = true;
            txtEasting.Text = 25 + (e.X)*(11.5/ 779) + "";
            txtNorthing.Text = 31.5 - (e.Y) * (9.5/643) + "";
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox1_MouseDown(object sender, MouseEventArgs e)
        {
            xMD = e.X;
            yMD = e.Y;
        }


        private void rbStreetMap_CheckedChanged(object sender, EventArgs e)
        {
            pictureBox1.Image = NDSS.Properties.Resources.Egypt_Streetmap;
        }

        private void rbSatImg_CheckedChanged(object sender, EventArgs e)
        {
            pictureBox1.Image = NDSS.Properties.Resources.Egypt_Basemap_High_quality;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Rectangle currentRec = new Rectangle(0, 0, 500, 500);
            Rectangle disRec = new Rectangle(0, 0, 500, 500);

            Bitmap myMap = new Bitmap(500,500);
            CopyRegionIntoImage((Bitmap)pictureBox1.Image, currentRec, ref myMap, disRec);
            myMap.Save(@"C:\Users\Public\DSS\trial.png");
        }
    }
}
