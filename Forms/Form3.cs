﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Drawing.Drawing2D;
using Word = Microsoft.Office.Interop.Word;




namespace NDSS.Forms
{
    public partial class Form3 : Form
    {
        private double[] values = { 0, 0.05, 0.1, 0.15, 0.2, 0.25, 0.3, 0.35, 0.4, 0.45, 0.5, 0.55, 0.6, 0.65, 0.7, 0.75, 0.8, 0.85, 0.9, 0.95, 1 };

        int count = 0;
        int transValue = 250;
        List<string>[] inputData;
        List<string> ids;
        List<string> DTW;
        List<string> TDS;
        List<string> Slope;
        List<string> ET;
        List<string> Sus;
        List<string> Existing;
        List<string> currentData;
        bool isGridOn = false;



        Point SelectedPixel = new Point(-9, -9);

        public Form3()
        {
            InitializeComponent();
            inputData = readMultiColumn(NDSS.Properties.Resources.input_realData4, 6);
            ids = idReader(@"C:\Users\Public\DSS\extracted_ids.csv");

            DTW = inputData[0];
            TDS = inputData[1];
            Slope = inputData[2];
            ET = inputData[3];
            Sus = inputData[4];
            Existing = inputData[5];

            using (var bmpTemp = new Bitmap(@"C:\Users\Public\DSS\extracted_img_Sat.png"))
            {
                double inputWidth = bmpTemp.Width;
                double inputHeight = bmpTemp.Height;

                if (inputWidth > inputHeight)
                {
                    pictureBox1.Height = (int)((inputHeight / inputWidth) * pictureBox1.Width);
                    pictureBox2.Height = pictureBox1.Height;

                    Point newLocation = new Point(pictureBox1.Location.X, pictureBox1.Location.Y + 250 - (int)(pictureBox1.Height / 2));

                    pictureBox1.Location = newLocation;
                    pictureBox2.Location = newLocation;
                }

                if (inputHeight > inputWidth)
                {
                    pictureBox1.Width = (int)((inputWidth / inputHeight) * pictureBox1.Height);
                    pictureBox2.Width = pictureBox1.Width;

                    Point newLocation = new Point(pictureBox1.Location.X + 250 - (int)(pictureBox1.Width / 2), pictureBox1.Location.Y);

                    pictureBox1.Location = newLocation;
                    pictureBox2.Location = newLocation;
                }

            }

            btnExport.BackColor = ColorTranslator.FromHtml(ThemeColor.ColorList[13]);
            btnCreateWord.BackColor = ColorTranslator.FromHtml(ThemeColor.ColorList[13]);

            updateImage(DTW, TDS, Slope, ET, Sus, Existing, ids);

            pixelBox1.Parent = pictureBox1;
            pixelBox1.Location = new Point(pictureBox1.Width - pixelBox1.Width, 0);

            pictureBox1.Parent = pictureBox2;
            pictureBox1.Location = new Point(0, 0);
        }



        public List<string> idReader(string filePath)
        {
            if (!Directory.Exists(@"C:\Users\Public\DSS"))
            {
                MessageBox.Show("Please select area first");
                List<string> emptyString = new List<string>();
                for (int i = 0; i < 400; i++)
                {
                    emptyString.Add("43600");
                }
                return emptyString;
            }
            using (var reader = new StreamReader(filePath))
            {
                List<string> idValues = new List<string>();

                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();

                    var values = line.Split(',');
                    if (line != "")
                    {
                        idValues.Add(values[0]);
                    }

                }
                return idValues;
            }
        }

        public List<string>[] readMultiColumn(string myData, int numberOfColumns)
        {
            using (var reader = new StringReader(myData))
            {
                List<string>[] totalData = new List<string>[numberOfColumns];

                for (int i = 0; i < numberOfColumns; i++)
                {
                    totalData[i] = new List<string>();
                }

                String line;
                while ((line = reader.ReadLine()) != null)
                {
                    var values = line.Split(',');
                    if (line != "")
                    {
                        for (int i = 0; i < numberOfColumns; i++)
                        {
                            totalData[i].Add(values[i]);
                        }
                    }

                }
                return totalData;
            }
        }

        private Color getColorFromValue(double value, int trans)
        {

            return Color.FromArgb(0, 255, 255, 255);
        }

        public void createImage()
        {
            int width = 10, height = 10;

            Bitmap bmp = new Bitmap(width, height);

            Random rand = new Random();

            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    int a = 255;
                    int r = rand.Next(256);
                    int g = rand.Next(256);
                    int b = rand.Next(256);

                    bmp.SetPixel(x, y, Color.FromArgb(a, 23, 179, 77));

                }
            }
            pictureBox1.Image = bmp;
        }



        private Bitmap MergeTwoImages(Image large_Image, Image small_Image)
        {
            Bitmap largeImage = (Bitmap)large_Image;
            int w1 = largeImage.Width;
            int h1 = largeImage.Height;
            Bitmap smallImage = (Bitmap)small_Image;
            int w2 = smallImage.Width;
            int h2 = smallImage.Height;
            Bitmap mergedImage = new Bitmap(w1, h1);
            for (int i = 0; i < w1; i++)
            {
                for (int j = 0; j < h1; j++)
                {
                    float ni = ((float)w2 / w1) * (i);
                    float nj = ((float)h2 / h1) * (j);
                    int i_small = (int)ni;
                    int j_small = (int)nj;
                    Color coverColor = smallImage.GetPixel(i_small, j_small);
                    Color oldColor = largeImage.GetPixel(i, j);
                    //int alpha = (oldColor.A * coverColor.A) / 255;
                    //int red = (oldColor.R * coverColor.R) / 255;
                    //int green = (oldColor.G * coverColor.G) / 255;
                    //int blue = (oldColor.B * coverColor.B) / 255;

                    mergedImage.SetPixel(i, j, Color.FromArgb(oldColor.A, oldColor.R, oldColor.G, oldColor.B));
                }
            }
            Graphics gr = Graphics.FromImage(mergedImage);
            float scaleX = (float)mergedImage.Width / pictureBox1.Image.Width;
            float scaleY = (float)mergedImage.Height / pictureBox1.Image.Height;

            Pen blackPen = new Pen(Color.FromArgb(255, 0, 0, 0), 3);
            Rectangle r = new Rectangle(new Point((int)(SelectedPixel.X * scaleX), (int)(SelectedPixel.Y * scaleY)), new Size((int)scaleX, (int)scaleY));
            gr.DrawRectangle(blackPen, r);
            //Point[] myPts = { new Point(1, 1), new Point(40, 200), new Point(100, 300) };
            //gr.DrawPolygon(blackPen,myPts);

            return mergedImage;
        }

        private void btnExport_Click_1(object sender, EventArgs e)
        {
            double topCoordinates;
            double leftCoordinates;
            using (var reader = new StreamReader(@"C:\Users\Public\DSS\" + "last_selection_Top_Left.csv"))
            {
                var values = reader.ReadLine().Split(',');
                topCoordinates = Convert.ToDouble(values[0]) - 0.025;
                leftCoordinates = Convert.ToDouble(values[1]) + 0.025;
            }
            using (SaveFileDialog saveFileDialog = new SaveFileDialog() { Filter = @"PNG|*.png" })
            {
                if (saveFileDialog.ShowDialog() == DialogResult.OK)
                {
                    pictureBox1.Image.Save(saveFileDialog.FileName);
                    using (var tw = new StreamWriter(saveFileDialog.FileName.Replace("png", "pgw")))
                    {
                        tw.WriteLine("0.05");
                        tw.WriteLine("0.000000000");
                        tw.WriteLine("0.000000000");
                        tw.WriteLine("-0.05");
                        tw.WriteLine(leftCoordinates);
                        tw.WriteLine(topCoordinates);
                    }
                    using (var twn = new StreamWriter(saveFileDialog.FileName.Replace("png", "prj")))
                    {
                        twn.WriteLine("GEOGCS[\"GCS_WGS_1984\", DATUM[\"D_WGS84\", SPHEROID[\"WGS84\", 6378137, 298.257223563]], PRIMEM[\"Greenwich\", 0], UNIT[\"Degree\", 0.017453292519943295]]");

                    }
                }
            }
        }



        private Bitmap updateImage(List<string> f1, List<string> f2, List<string> f3, List<string> f4, List<string> f5, List<string> f6, List<string> ids)
        {
            if (rbSatImg.Checked)
            {
                using (var bmpTemp = new Bitmap(@"C:\Users\Public\DSS\extracted_img_Sat.png"))
                {
                    pictureBox2.Image = new Bitmap(bmpTemp);
                }
            }

            if (rbStreetMap.Checked)
            {
                using (var bmpTemp = new Bitmap(@"C:\Users\Public\DSS\extracted_img_Street.png"))
                {
                    pictureBox2.Image = new Bitmap(bmpTemp);
                }
            }

            List<Color> pixelColors = new List<Color>();

            for (int i = 0; i < ids.Count; i++)
            {


                pixelColors.Add(getColorFromValue(1, transValue));
            }

            int width = (int)(pictureBox2.Image.Width / 37.282), height = (int)(pictureBox2.Image.Height / 37.282);

            Bitmap bmp = new Bitmap(width, height);
            Bitmap bmpGrid = new Bitmap(pictureBox2.Image.Width, pictureBox2.Image.Height);

            float scaleX = (float)bmpGrid.Width / bmp.Width;
            float scaleY = (float)bmpGrid.Height / bmp.Height;

            int counter = 0;
            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    if (SelectedPixel.X == x && SelectedPixel.Y == y)
                    {
                        int index = Convert.ToInt32(ids[counter]);

                        txtProp1.Text = f1[index] + "";
                        txtProp2.Text = f2[index] + "";
                        txtProp3.Text = f3[index] + "";
                        txtProp4.Text = f4[index] + "";
                        txtProp5.Text = f5[index] + "";
                        txtProp6.Text = f6[index] + "";
                    }
                    Color c = pixelColors[counter];
                    bmp.SetPixel(x, y, c);
                    counter++;
                    if (isGridOn)
                    {
                        Graphics gr = Graphics.FromImage(pictureBox2.Image);
                        Pen brownPen = new Pen(Color.FromArgb(100, 119, 136, 153), 2);
                        //Pen pp = new Pen(Color.
                        Rectangle r = new Rectangle(new Point((int)(x * scaleX), (int)(y * scaleY)), new Size((int)scaleX, (int)scaleY));
                        gr.DrawRectangle(brownPen, r);
                        //gr.DrawRectangle(brownPen, x * scaleX, y * scaleY, scaleX, scaleY);
                        int yu = 4;
                    }


                }
            }

            pictureBox1.Image = bmp;
            pictureBox1.Parent = pictureBox2;
            pictureBox1.BackColor = Color.Transparent;
            pictureBox1.Location = new Point(0, 0);

            return bmp;

        }

        public void createGrid()
        {
            Bitmap bmp = new Bitmap(pictureBox1.Image.Width, pictureBox1.Image.Height);
            Bitmap bmpGrid = new Bitmap(pictureBox1.ClientSize.Width, pictureBox1.ClientSize.Height);

            float scaleX = (float)bmpGrid.Width / bmp.Width;
            float scaleY = (float)bmpGrid.Height / bmp.Height;


            for (int x = 0; x < 10; x++)
            {
                for (int y = 0; y < 10; y++)
                {
                    if (isGridOn)
                    {
                        Graphics gr = Graphics.FromImage(pictureBox2.Image);
                        Pen brownPen = new Pen(Color.FromArgb(255, 119, 136, 153), 1);
                        Rectangle r = new Rectangle(new Point((int)(x * scaleX), (int)(y * scaleY)), new Size((int)scaleX, (int)scaleY));
                        gr.DrawRectangle(brownPen, r);
                        //gr.DrawRectangle(brownPen, x * scaleX, y * scaleY, scaleX, scaleY);
                        int yu = 4;
                    }


                }
            }


        }



        private void pictureBox1_MouseUp(object sender, MouseEventArgs e)
        {
            //Bitmap b = new Bitmap(pictureBox1.ClientSize.Width, pictureBox1.ClientSize.Height);
            Bitmap b = new Bitmap(pictureBox1.Image);
            float nx = ((float)b.Width / pictureBox1.ClientSize.Width) * (e.X);
            float ny = ((float)b.Height / pictureBox1.ClientSize.Height) * (e.Y);

            Color color = b.GetPixel((int)nx, (int)ny);
            SelectedPixel.X = (int)nx;
            SelectedPixel.Y = (int)ny;
            Bitmap bmp = updateImage(DTW, TDS, Slope, ET, Sus, Existing, ids);

            pictureBox1.Image = bmp;
            pictureBox1.Parent = pictureBox2;
            pictureBox1.BackColor = Color.Transparent;
            pictureBox1.Location = new Point(0, 0);
            propPanel.Visible = true;
        }

        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {
            if (SelectedPixel.X != -9 && SelectedPixel.Y != -9)
            {
                float scaleX = (float)pictureBox1.ClientSize.Width / pictureBox1.Image.Width;
                float scaleY = (float)pictureBox1.ClientSize.Height / pictureBox1.Image.Height;
                Pen blackPen = new Pen(Color.FromArgb(255, 0, 0, 0), 3);
                e.Graphics.DrawRectangle(blackPen, SelectedPixel.X * scaleX, SelectedPixel.Y * scaleY, scaleX, scaleY);
                int s = 5;
            }
        }


        private void rbSatImg_CheckedChanged(object sender, EventArgs e)
        {
            pictureBox2.Image = Image.FromFile(@"C:\Users\Public\DSS\extracted_img_Sat.png");
            updateImage(DTW, TDS, Slope, ET, Sus, Existing, ids);
        }

        private void rbStreetMap_CheckedChanged(object sender, EventArgs e)
        {
            pictureBox2.Image = Image.FromFile(@"C:\Users\Public\DSS\extracted_img_Street.png");
            updateImage(DTW, TDS, Slope, ET, Sus, Existing, ids);

        }

        private void btnCreateWord_Click(object sender, EventArgs e)
        {
            Word.Application app = new Word.Application();

            String fileName = Path.GetTempFileName();
            File.WriteAllBytes(fileName, Properties.Resources.Template_DSS2);
            Word.Document doc = app.Documents.Open(fileName);

            app.ActiveDocument.Characters.Last.Select(); 
            app.Selection.Collapse();

            object control = 1;

            using (var reader = new StreamReader(@"C:\Users\Public\DSS\" + "last_selection.csv"))
            {
                reader.ReadLine();
                var values = reader.ReadLine().Split(',');

                doc.SelectContentControlsByTag("right").get_Item(ref control).Range.Text = values[0];
                doc.SelectContentControlsByTag("left").get_Item(ref control).Range.Text = values[1];
                doc.SelectContentControlsByTag("bottom").get_Item(ref control).Range.Text = values[2];
                doc.SelectContentControlsByTag("top").get_Item(ref control).Range.Text = values[3];
            }

            DateTime today = DateTime.Today;
            doc.SelectContentControlsByTag("date").get_Item(ref control).Range.Text = today.ToString("MMM yyyy");
            doc.SelectContentControlsByTag("f1").get_Item(ref control).Range.Text = "Depth to water";
            doc.SelectContentControlsByTag("f2").get_Item(ref control).Range.Text = "TDS";
            doc.SelectContentControlsByTag("f3").get_Item(ref control).Range.Text = "Slope";
            doc.SelectContentControlsByTag("f4").get_Item(ref control).Range.Text = "Evapotranspiration";
            doc.SelectContentControlsByTag("f5").get_Item(ref control).Range.Text = "Sustainability";
            doc.SelectContentControlsByTag("f6").get_Item(ref control).Range.Text = "Existing development";

            doc.SelectContentControlsByTag("v1").get_Item(ref control).Range.Text = txtProp1.Text;
            doc.SelectContentControlsByTag("v2").get_Item(ref control).Range.Text = txtProp2.Text;
            doc.SelectContentControlsByTag("v3").get_Item(ref control).Range.Text = txtProp3.Text;
            doc.SelectContentControlsByTag("v4").get_Item(ref control).Range.Text = txtProp4.Text;
            doc.SelectContentControlsByTag("v5").get_Item(ref control).Range.Text = txtProp5.Text;
            doc.SelectContentControlsByTag("v6").get_Item(ref control).Range.Text = txtProp6.Text;


            //doc.SelectContentControlsByTag("b").get_Item(ref control).Range.Text = "This text added to B";
            //doc.SelectContentControlsByTag("c").get_Item(ref control).Range.Text = "This text added to C";

            MergeTwoImages(pictureBox2.Image,pictureBox1.Image).Save(@"C:\Users\Public\DSS\development_areas.png");

            doc.SelectContentControlsByTag("pic1").get_Item(ref control).Range.InlineShapes.AddPicture(@"C:\Users\Public\DSS\development_areas.png");

            using (SaveFileDialog saveFileDialog = new SaveFileDialog() { Filter = @"Word Document |*.docx" })
            {
                if (saveFileDialog.ShowDialog() == DialogResult.OK)
                {
                    app.ActiveDocument.SaveAs2(saveFileDialog.FileName);
                    app.Quit();
                    File.Delete(fileName);
                }
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void Form3_Load(object sender, EventArgs e)
        {

        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked)
            {
                isGridOn = true;
                updateImage(DTW, TDS, Slope, ET, Sus, Existing, ids);
            }
            if (!checkBox1.Checked)
            {
                isGridOn = false;
                updateImage(DTW, TDS, Slope, ET, Sus, Existing, ids);
            }


        }
    }
}
